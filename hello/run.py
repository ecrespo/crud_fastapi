import uvicorn


def main():
    # logger.info("Running movie-service")

    uvicorn.run("main:app", host="0.0.0.0", port=9000, reload=True, workers=4)


if __name__ == "__main__":
    main()
